#!/usr/bin/perl
package runner;
#use Exporter;
#our @ISA = qw(Exporter);

use strict;
use IPC::Open2;
use IO::Handle;

# Global part

my $lineSeparator = "__this_is_a_line_separator__";
my $defaultConfName = "ssh";

#TODO : replace configurations below with a single hash

my %connectUser;
my %relayHost;
my %relayUser;
my %relayCmd;
my %connectCon;

# SSH default
# This is used for standard connexion. Everything is setup (~/.ssh.config) and from the shell you just issue "ssh host" to connect

$connectUser{ssh} = "";
$relayHost{ssh} = ""; 
$relayUser{ssh} = ""; 
$relayCmd{ssh} = ""; 
$connectCon{ssh} = "/usr/bin/ssh -t";

# Direct connection
# This is when you conect directly to host, but you need to specify the user. From bash, you do "ssh user@host"

$connectUser{direct} = ""; # login on the remote host
$relayHost{direct} = ""; 
$relayUser{direct} = ""; 
$relayCmd{direct} = ""; 
$connectCon{direct} = "/usr/bin/ssh -t -l $connectUser{direct}";

# Connection using a relay host
# This is particular configuration when you have to use a relay host to reach the server.

$connectUser{relay} = ""; # login on the remote host
$relayHost{relay} = "";   # name of the bastion/relay host
$relayUser{relay} = $ENV{USER}; # login on the bastion/relay host
$relayCmd{relay} = "ssh -t -l $relayUser{relay} $relayHost{relay}";
$connectCon{relay} = "sudo -H /usr/bin/ssh -t -l $connectUser{relay}";

# Back to Global

sub connectCmd {
  my $connectConfName = $_[0];
  my $connectCmd = $relayCmd{$connectConfName}." ". $connectCon{$connectConfName};
  return $connectCmd;
}

=head1 NAME

  runner - ssh connection to a host

=head1 SYNOPSIS

  $host = runner->new("hostname","conf");
  $host->connect;
  $host->send("ls");
  foreach ($host->get) {
    print $_."\n";
  }
  $host->disconnect;

=head1 DESCRIPTION

  runner connects a host with ssh and let you send commands, expecting result as if it was a terminal.

  As long as runner is intended to launch scripts on multiple hosts, we assume ssh connexion are set up without a password to be prompted for.

=cut

BEGIN { # Module initiator

}

END { # Module terminator

}

=head1 FUNCTION new

  This function constructs a new object. Host name is to be passed as an argument.

=cut

sub new {
  my ($class, @initValues) = @_;
  my $this = {};
  bless ($this, $class);

  (my $hostName, my $confName, @initValues) = @initValues;
  $this->{hostName} = $hostName;
  $this->{confName} = $confName;
  $this->{confName} = $defaultConfName if not defined $confName;
  $this->{connected} = 0; # initialy not connected;

  return $this;
}

=head1 FUNCTION connect

  This is where connection is established. No argument expected, as long as hostname is already set.

=cut

sub connect {
  my ($this) = @_;
  my $hostName = $this->{hostName};
  my $confName = $this->{confName};
  my $connectCmd=connectCmd($confName);
  my $connectString = $connectCmd." ".$hostName." 2>&1";
  (my $writeFh, my $readFh) = (IO::Handle->new(),IO::Handle->new());
  my $childPid = open2($readFh,$writeFh,$connectString);
  # wait for connection establishement
  sleep 1;
  $writeFh->print("echo $lineSeparator\n");
  while (<$readFh>) {
    chomp;
    last if /$lineSeparator/;
  }
  $this->{connected} = 1;
  $this->{writeFh} = $writeFh;
  $this->{readFh} = $readFh;
  $this->{childPid} = $childPid;
}

=head1 FUNCTION disconnect

  Ends up the connection properly. If not called explicitly, will be executed on destruction.

=cut

sub disconnect {
  my ($this) = @_;
  my $writeFh = $this->{writeFh};
  my $readFh = $this->{readFh};
  my $childPid = $this->{childPid};
  $writeFh->print("exit");
  $writeFh->close();
  $readFh->close();
  waitpid ($childPid,0);
  my $childExitStatus = $? >> 8; 
  $this->{connected} = 0;
  return $childExitStatus;
}

=head1 FUNCTION send

  Send some command to the distant host

=cut

sub send {
  (my $this, my @commandLines) = @_;
  my $writeFh = $this->{writeFh};
  foreach (@commandLines) {
    chomp;
    $writeFh->print($_."\n");
  }
}

=head1 FUNCTION get

  Get some output from previously sent commands.

=cut

sub get {
  my ($this) = @_;
  my @outputLines;
  my $readFh = $this->{readFh};
  $this->send("echo $lineSeparator");
  while (<$readFh>) {
    chomp;
    last if /$lineSeparator/;
    @outputLines=(@outputLines,$_);
  }
  return @outputLines;
}


sub DESTROY {
  my ($this) = @_;
  $this->disconnect() if $this->{connected} == 1;
}

1;

