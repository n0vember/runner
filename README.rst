runner usage
============

runner is a script used to launch a set of commands on a set of hosts and eventually get the result.

It can take the folowing arguments :

- -h => gives help
- -v => verbose mode activation
- -c config => sets how connexion is made to the hosts
- -H hosts_file => list of hosts to connect to
- -C commands_file => list of commands to send to each host

configuration names are described in the module and the documentation. They are :

- ssh => This is the default and does not need to be specified. Will issue a "ssh host" on each host.
- direct => This will issue a "ssh user@host" on each host.
- relay => Uses a relay host. See in the module how to use.

default hosts_file name is hosts.lst

default commands_file name is commands.lst

Restriction : each command in commands_file must be a oneliner.

hosts file
---------

It simply is a file containing a list of hosts. One line = one host.

commands file
-------------

As simple as hosts file. Theorically.

Each command to be sent must stay on one line. If commands are continued on the next line, each line will be sent as single commands and result may vary from script frozen to missing parameters and commands destroying things on multiple hosts... Be careful : test your commands, start small.

execution example
-----------------

content of file hosts_file.txt::

 myserver
 localhost

content of file hosts.cmd::

 hostname -f

command used to launch::

 ./runner -H hosts_file.txt -C hosts.cmd

output::

 myserver.domain.com
 localhost.localdomain

by renaming hosts_file.txt and hosts.cmd respectively hosts.lst and commands.lst, the command would have simply been::

 ./runner

runner.pm documentation
=======================

Below is the documentation for the runner.pm module. This module can be used appart from runner. It is used to communicate with a host via an ssh connection kept open.

::
 NAME
       runner - ssh connection to a host
 
 SYNOPSIS
       $host = runner->new("hostname","conf");
       $host->connect;
       $host->send("ls");
       foreach ($host->get) {
         print $_."\n";
       }
       $host->disconnect;
 
 DESCRIPTION
       runner connects a host with ssh and let you send commands, expecting result as if it was a terminal.
 
       As long as runner is intended to launch scripts on multiple hosts, we assume ssh connexion are set up without a password to be prompted for.
 
 FUNCTION new
       This function constructs a new object. Host name is to be passed as an argument.
 
 FUNCTION connect
       This is where connection is established. No argument expected, as long as hostname is already set.
 
 FUNCTION disconnect
       Ends up the connection properly. If not called explicitly, will be executed on destruction.
 
 FUNCTION send
       Send some command to the distant host
 
 FUNCTION get
       Get some output from previously sent commands.
 
TODO
====

- being able to use an ansible inventory
- use a configuration file for the connections type of runner.pm
- use a single hash for the connections in runner.pm

